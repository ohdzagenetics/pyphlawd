# A Docker container for running phyluce
FROM conda/miniconda2

ENV MCL_VERSION "14-137"
ENV FAST_TREE_VERSION "2.1.10"
ENV CONF_PY="src/conf.py"


# Install dependencies from available repos
RUN apt-get update && apt-get install -y \
	build-essential \
	mafft \
	ncbi-blast+ \
	openmpi-common


# Install MCL, Markov Cluster Algorithm
ADD https://micans.org/mcl/src/mcl-$MCL_VERSION.tar.gz \
	/tmp/mcl.tar.gz
RUN cd /tmp/ && \
	tar zxf mcl.tar.gz --strip-components=1 && \
	./configure && \
	make install && \
	cd / && \
	rm -rf /tmp/


# Install FastTree
ADD http://www.microbesonline.org/fasttree/FastTree-$FAST_TREE_VERSION.c \
	/tmp/FastTree.c
RUN cd /tmp/ && \
	gcc -DOPENMP -fopenmp -O3 -finline-functions -funroll-loops -Wall -o FastTree FastTree.c -lm && \
	mv FastTree /usr/local/bin/FastTree && \
	cd / && \
	rm -rf /tmp/
	
# Ensure python dependencies are installed
# sqlite3 is a dependency that should be in the standard library
RUN pip install \
	clint \
	Cython \
	networkx

# Add PyPHLAWD source code
ADD ./src/ /PyPHLAWD/
WORKDIR /PyPHLAWD/
ADD $CONF_PY conf.py
RUN bash compile_cython.sh
